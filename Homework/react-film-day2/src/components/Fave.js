import React, { Component } from "react";

class Fave extends Component {
  state = {
    isFave: false
  };

  handleClick = e => {
    e.stopPropagation();
    this.setState({ isFave: !this.state.isFave });
  };

  render() {
    const addClass = this.state.isFave ? "remove_from_queue" : "add_to_queue";

    return (
      <div className={`film-row-fave ${addClass}`} onClick={this.handleClick}>
        <p className="material-icons">{addClass}</p>
      </div>
    );
  }
}

export default Fave;
